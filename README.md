# 两级分销加两级代理佣金计算工具

#### 介绍
html+vue.js自动计算用户佣金工具



-   这里是列表文本 标题 标题两级分销:普通花粉(75折,25%的佣金),白金花粉(7折,30%佣金)
-   这里是列表文本 两级代理:钻石花粉(65折,35%佣金),花花董事(55折,45%佣金)
-   极差佣金:上级的佣金=上级可得佣金减去下级已得佣金(其中6%的同级佣金并不计算)
-   比如,我是花花董事,我推荐了个99元的礼包,我拿45%,如果是我下级的普通花粉推荐出去的,那么我拿99*(45%-25%)
-   参考博客:[https://blog.ibiaoqin.cn/arc/2.html](https://blog.ibiaoqin.cn/arc/2.html)
-   案例网址:[https://blog.ibiaoqin.cn/fx/hfjs.html](https://blog.ibiaoqin.cn/fx/hfjs.html)
-   使用视频:[https://mp.weixin.qq.com/s/PHCMgp-AFGXWB88YZetNTQ](https://mp.weixin.qq.com/s/PHCMgp-AFGXWB88YZetNTQ)




#### 使用说明



- 所有代理分销拿极差的佣金,两级代理有同级佣金6
- 商品佣金需要配合婲坊商城进行使用,并且打乱了佣金比例
- 佣金比例参考下图,



![输入图片说明](https://images.gitee.com/uploads/images/2021/0227/143846_017846c0_4980182.jpeg "微信截图_20210227143331.jpg")


#### 特技

其它商品暂不支持,仅提供99元礼包的计算
